package slice.console;

import slice.slice.*;
import slice.resource.AllocationException;

import org.simpleframework.http.Response;
import org.simpleframework.http.Request;
import org.simpleframework.http.Path;
import org.simpleframework.http.Query;
import us.monoid.json.*;

import java.util.Scanner;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.io.PrintStream;
import java.io.IOException;

import cc.clayman.console.*;

import cc.clayman.logging.*;

/**
 * A class to handle /slice/ requests
 */
public class SliceHandler extends BasicRequestHandler implements RequestHandler {
    SliceControllerConsole console;


    public SliceHandler() {
        // allocate a new logger
        Logger logger = Logger.getLogger("log");

        logger.addOutput(System.err, new BitMask(MASK.ERROR));
        logger.addOutput(System.out, new BitMask(MASK.STDOUT));

    }


    /**
     * Handle a request and send a response.
     */
    public boolean  handle(Request request, Response response) {
        // get RestListener
        console = (SliceControllerConsole)getManagementConsole();

        try {
            /*
            System.out.println("method: " + request.getMethod());
            System.out.println("target: " + request.getTarget());
            System.out.println("path: " + request.getPath());
            System.out.println("directory: " + request.getPath().getDirectory());
            System.out.println("name: " + request.getPath().getName());
            System.out.println("segments: " + java.util.Arrays.asList(request.getPath().getSegments()));
            System.out.println("query: " + request.getQuery());
            System.out.println("keys: " + request.getQuery().keySet());
            */

            //System.out.println("\n/slice/ REQUEST: " + request.getMethod() + " " +  request.getTarget());

            long time = System.currentTimeMillis();

            response.set("Content-Type", "application/json");
            response.set("Server", "Slice Controller/1.0 (SimpleFramework 4.0)");
            response.setDate("Date", time);
            response.setDate("Last-Modified", time);

            // get the path
            Path path =  request.getPath();
            String directory = path.getDirectory();
            String name = path.getName();
            String[] segments = path.getSegments();

            // Get the method
            String method = request.getMethod();

            // Get the Query
            Query query = request.getQuery();


            // and evaluate the input
            if (method.equals("POST")) {
                createSlice(request, response);
                
            } else if (method.equals("DELETE")) {
                deleteSlice(request, response);
                
            } else if (method.equals("GET")) {
                if (name == null) {      // no arg, so get energy data
                    listSlices(request, response);
                } else if (segments.length == 2) {   // get specific energy info
                } else {
                    notFound(response, "GET bad request");
                }
                
            } else if (method.equals("PUT")) {
                updateData(request, response);
                
            } else {
                badRequest(response, "Unknown method" + method);
            }



            // check if the response is closed
            response.close();

            return true;

        } catch (IOException ioe) {
            System.err.println("IOException " + ioe.getMessage());
        } catch (JSONException jse) {
            System.err.println("JSONException " + jse.getMessage());
        }

        return false;

    }

    /**
     * GET: Get some data given a request and send a response.
     */
    public void listSlices(Request request, Response response) throws IOException, JSONException  {
        Logger.getLogger("log").logln(MASK.STDOUT, "REST Slice Handler: " +  "listSlices");

        // process query

        Query query = request.getQuery();

        // and send them back as the return value
        PrintStream out = response.getPrintStream();

        JSONObject jsobj = new JSONObject();

        // get the list from the console
        List<Slice> slices = console.getController().getSlices();

        // {
        //     message: "list-slice"
        //     timestamp: 449498234292
        //     payload: {
	//         type: "Wh" 
        //         value: 4612.34
        //      }
        // }

        jsobj.put("message", "list-slice");
        jsobj.put("timestamp", System.currentTimeMillis());

        JSONObject payload = new JSONObject();

        JSONArray list = new JSONArray();
        
        for (Slice slice: slices) {
            list.put(slice.getID());
        }

        payload.put("list", list);

        jsobj.put("payload", payload);

        out.println(jsobj.toString());
    }

    /**
     * POST: create a slice
     * Equivalent of:  curl -X POST -d '{"size":"1","type":"vlsp"}' http://localhost:7080/slice/
     */
    public void createSlice(Request request, Response response) throws IOException, JSONException  {
        Logger.getLogger("log").logln(MASK.STDOUT, "REST Slice Handler: " +  "createSlice");

        Query query = request.getQuery();

        JSONObject recvd;
        

        if (query.size() == 0) {
            String content = request.getContent();

            if (content != null && content.length() > 0) {
            
                JSONString value = new JSONData(content);
            
                //Path path =  request.getPath();
                //System.err.println("\nGot: " + path + " '" + value + "'");

                recvd = new JSONObject(value.toString());

                System.err.println("recvd = " + recvd + " ");
                        
            } else {
                complain(response, "No request value passed in");
                return;
            }

        } else {
            complain(response, "No passed POSTed in");
            return;
        }


        // Create the slice
        SliceCreator creator = new SliceCreator(console.getController());
        Slice slice;

        // try and create using the passed in values
        // it should look like  '{"size":"2","type":"vlsp"}'
        try {
            slice = creator.create(recvd);

            if (slice == null) {
                complain(response, "Cannot allocate");
                return;
            }

        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
            // recvd has junk in it
            complain(response, iae.getMessage());
            return;
                
        } catch (AllocationException ae) {
            ae.printStackTrace();
            // not enough hosts
            complain(response, ae.getMessage());
            return;
        }

        // register the slice with the Controller
        console.getController().registerSlice(slice);
    
        // and send them back as the return value
        PrintStream out = response.getPrintStream();

        JSONObject jsobj = new JSONObject();

        // get the list from the console

        jsobj.put("message", "create-slice");
        jsobj.put("timestamp", System.currentTimeMillis());

        JSONObject payload = new JSONObject();

        payload.put("id", slice.getID());
        payload.put("size", slice.getSize());
        payload.put("type", slice.getType());

        JSONObject vim = new JSONObject();
        vim.put("hostname", slice.getVimHost().getName());
        vim.put("port", slice.getVimHost().getPort());

        payload.put("vim", vim);

        jsobj.put("payload", payload);

        out.println(jsobj.toString());
    }
    
    /**
     * PUT: Update some data given a request and send a response.
     */
    public void updateData(Request request, Response response) throws IOException, JSONException  {
        Logger.getLogger("log").logln(MASK.STDOUT, "REST Slice Handler: " +  "updateData");

        // see if we should send xml
        boolean xml = false;

        boolean hasHTTPAccept = request.contains("Accept");

        if (hasHTTPAccept) {
            String accept = request.getValue("Accept");
            if (accept.equals("application/xml") || accept.equals("text/xml")) {
                xml = true;
            }
        }

        // get the path
        Path path =  request.getPath();
        String name = path.getName();

        Query query = request.getQuery();
        Scanner scanner;

        /* process  args */


        // process value arg
        JSONString value = null;
        Double price = 0.0;
        boolean success = true;
        String failMessage = null;
        
        if (query.containsKey("value")) {
            try {
                String valueArg = query.get("value");
                price = Double.parseDouble(valueArg);
            } catch (Exception e) {
                complain(response, "Price value not a float or double passed in");
                return;
            }

        } else {
            String content = request.getContent();

            if (content != null && content.length() > 0) {
            
                value = new JSONData(content);
            
                //System.err.println("\nGot: " + path + " '" + value + "'");

                JSONObject recvd = new JSONObject(value.toString());

                //System.err.println("recvd = " + recvd + " ");
        
                JSONObject payload = recvd.getJSONObject("payload");
                price = payload.getDouble("price");
                
            } else {
                complain(response, "No update value passed in");
                return;
            }

        }
        


        System.err.println("Recv price: " + price);

        // set the price into the console
        //// console.setEnergyPrice(price);


        // and send them back as the return value
        PrintStream out = response.getPrintStream();

        JSONObject jsobj = new JSONObject();

        jsobj.put("value", price);
        jsobj.put("result", "OK");

        if (xml) {
            out.println(XML.toString(jsobj, "response"));
        } else {                       
            out.println(jsobj.toString());
        }


    }

    /**
     * DELETE: delete a slice
     */
    public void deleteSlice(Request request, Response response) throws IOException, JSONException  {
        Logger.getLogger("log").logln(MASK.STDOUT, "REST Slice Handler: " +  "deleteSlice");

        // if we got here we have 2 parts
        // /slice/ and another bit
        String name = request.getPath().getName();
        Scanner sc = new Scanner(name);


        if (sc.hasNextInt()) {
            int id = sc.nextInt();
            sc.close();
            // if it exists, stop it, otherwise complain
            if (console.getController().isValidSliceID(id)) {
                // delete a Slice
                boolean success = console.getController().releaseSlice(id);

                if (success) {
                    // now lookup all the saved details
                    // and send them back as the return value
                    // and send them back as the return value
                    PrintStream out = response.getPrintStream();

                    JSONObject jsobj = new JSONObject();

                    // get the list from the console

                    jsobj.put("message", "delete-slice");
                    jsobj.put("timestamp", System.currentTimeMillis());

                    JSONObject payload = new JSONObject();

                    payload.put("id", id);

                    jsobj.put("payload", payload);

                    out.println(jsobj.toString());

                } else {
                    complain(response, "Error deleting Slice: " + id);
                }

            } else {
                complain(response, "deleteRouter arg is not valid slice id: " + name);
            }

        } else {
            complain(response, "deleteRouter arg is not Integer: " + name);
        }
    }


}

class JSONData extends JSONObject implements JSONString {

    public JSONData() throws JSONException {
        super();
    }

    public JSONData(String s) throws JSONException {
        super(s);
    }

    public JSONData(JSONObject js) throws JSONException {
        super(js, JSONObject.getNames(js));

    }


    public String toJSONString() {
        return super.toString();
    }
}
    
