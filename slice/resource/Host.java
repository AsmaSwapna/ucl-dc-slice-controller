package slice.resource;

/**
 * A Host
 */
public class Host {
    // The name of the host
    String name;

    public Host(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }


    public boolean equals(Object other) {
        if (other instanceof Host) {
            Host otherHP = (Host)other;
            
            if ((name.equals(otherHP.name))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public int hashCode() {
        return name.hashCode();
    }

    public String toString() {
        return "<" + getName() + ">";
    }
}
