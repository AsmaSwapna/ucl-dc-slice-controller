package slice.resource;

/**
 * A HostPort
 */
public class HostPort extends Host {
    int port;

    public HostPort(String name, int port) {
        super(name);
        this.port = port;
    }

    public HostPort(String name) {
        super(name);
    }

    public HostPort(Host host) {
        super(host.name);
    }

    public HostPort(Host host, int port) {
        super(host.name);
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public HostPort setPort(int p) {
        port = p;
        return this;
    }

    public boolean equals(Object other) {
        if (other instanceof HostPort) {
            HostPort otherHP = (HostPort)other;
            
            if ((name.equals(otherHP.name)) && (port == otherHP.port)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public int hashCode() {
        return name.hashCode() + port;
    }
    
    public String toString() {
        return "<" + getName() + ":" + getPort() + ">";
    }
}
