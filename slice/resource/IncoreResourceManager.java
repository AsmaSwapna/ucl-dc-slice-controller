package slice.resource;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

public class IncoreResourceManager extends AbstractResourceManager implements ResourceManager {

    // The host list for slice0
    // This is used for placing VIMs
    ArrayList<Host> slice0;   // set in config
    ArrayList<Integer>slice0Ports; // set in config


    ArrayList<HostPort>usedSlice0;

    // The Placement Manager for VIMs
    // It takes the slice0 hosts and works out where to place a VIM
    VimPlacementManager vimManager;

    // The host list for all the other hosts
    ArrayList<Host> hosts;    // set in config

    // The list of used hosts
    ArrayList<Host> used;

    // The list of free hosts
    ArrayList<Host> free;

    // A Map of list of hosts that are in the process of being allocated
    Map<Integer, ArrayList<Host>> allocating;

    String config = null;
    JSONObject jsConfig = null;

    public IncoreResourceManager() {
        config = DEFAULT_CONFIG;
    }

    public IncoreResourceManager(String confVal) throws IOException {
        config = confVal;
    }

    public IncoreResourceManager(JSONObject confVal) throws IOException {
        jsConfig = confVal;
    }


    public boolean start() {
        // Process the config
        try {
            if (config != null) {
                // A string was passed as config
                // so convert to JSONObject
                jsConfig = new JSONObject(config);
            }

            boolean processed = processConfig(jsConfig);

            if (!processed) {
                System.err.println("IncoreResourceManager config error: " + config);
                return false;
            }
            
        } catch (JSONException je) {
            System.err.println("IncoreResourceManager config error: " + config + " at " + je.getMessage());
            return false;
        }

        // Allocate a VimPlacementManager
        vimManager = new VimPlacementManager();
        // and set the hosts for slice0
        vimManager.setHosts(slice0);
            

        // Make all other hosts 'free' Hosts
        free = new ArrayList<Host>();
        
        for (Host h : hosts) {
            free.add(h);
        }

        // No 'used' hosts
        used = new ArrayList<Host>();

        // No hosts being allocated
        allocating = new HashMap<Integer,ArrayList<Host>>();

        // used slice0
        usedSlice0 = new ArrayList<HostPort>();

        System.out.println("slice0 = " + slice0);

        System.out.println("hosts = " + hosts);
        System.out.println("free = " + free);
        System.out.println("used = " + used);
        System.out.println("allocating = " + allocating);

        return true;
    }

    public boolean stop() {
        return true;
    }

    /**
     * How many hosts controlled by the ResourceManager
     */
    public int countHosts() {
        return hosts.size();
    }

    /**
     * How many free hosts controlled by the ResourceManager
     */
    public int countFreeHosts() {
        return free.size();
    }

    /**
     * How many used hosts controlled by the ResourceManager
     */
    public int countUsedHosts() {
        return used.size();
    }

    /**
     * Get a host to place a VIM
     */
    public HostPort getVimHost(int sliceSize, String sliceType) {
        // Ask the VimPlacementManager to find some hosts for a VIM
        Host hs = vimManager.findHost(sliceSize, sliceType);

        // now find a port
        int port = slice0Ports.get(0);

        HostPort hp = new HostPort(hs, port);
        
        return hp;
    }
        

    /**
     * Get a list of N hosts for a slice
     */
    public List<Host> allocate(int n) throws AllocationException {
        ArrayList<Host> thisAllocation = new ArrayList<Host>();

        try {
            long t0 = System.currentTimeMillis();
            
            // Wait to aquire a lock -- only one allocate at once
            //
            boolean acquired = semaphore.tryAcquire(semaphoreTimeout, TimeUnit.MILLISECONDS);

            if (!acquired) {
                throw new TimeoutException("IncoreResourceManager: " + " no acquire after " + semaphoreTimeout + " milliseconds");
            }

            if (n <= free.size()) {
                // find N free ones from free pool
                // put them in a temporary allocated pool
                // and return them
                for (int h=0; h<n; h++) {
                    // copy host from free to allocating
                    // take 0th elem
                    Host use = free.remove(0);
                    thisAllocation.add(use);
                }

                // mapping code -> list
                allocating.put(thisAllocation.hashCode(), thisAllocation);


                
                // might need to change return type to have a hashcode as well
                // the hashcode can be used in the confirmation
                // mayeb change List<Host> to a PreSlice object which contains all info

                // wait for the caller to confirm the allocation
                // to move the hosts to the used pool

            } else {
                // not enough hosts
                throw new AllocationException("Not enough hosts");
            }

            long t1 = System.currentTimeMillis();

        } finally {
            System.out.println("IncoreResourceManager allocating = " + allocating);


            semaphore.release();
            return thisAllocation;
        }

    }
        

    /**
     * Confirm a list of N hosts pre allocated for a slice.
     * The hosts are marked as being used.
     */
    public int confirm(int code) {
        List<Host> alloc = allocating.get(code);

        // remove from allocating
        allocating.remove(code);

        System.out.println("IncoreResourceManager allocating now = " + allocating);


        // now put hosts back in free pool
        int n = alloc.size();
        
        for (int h=0; h<n; h++) {
            // copy host from alloc to used
            // take 0th elem
            Host use = alloc.get(h);
            used.add(use);
        }

        return n;
       
    }

    /**
     * Release a list of N hosts pre allocated for a slice.
     * The hosts are put back in the free pool.
     */
    public int release(int code ) {
        List<Host> alloc = allocating.get(code);

        // remove from allocating
        allocating.remove(code);

        System.out.println("IncoreResourceManager allocating now = " + allocating);


        // now put hosts back in free pool
        int n = alloc.size();
        
        for (int h=0; h<n; h++) {
            // copy host from alloc to free
            // take 0th elem
            Host use = alloc.get(h);
            free.add(use);
        }

        return n;
        
    }

    /**
     * Free a list of N hosts which were allocated for a slice.
     * The hosts are put back in the free pool.
     */
    public int free(List<Host> allocation ) {
        System.out.println("IncoreResourceManager freeing = " + allocation);


        // now put hosts back in free pool
        int n = allocation.size();
        
        for (int h=0; h<n; h++) {
            // copy host from alloc to free
            // take 0th elem
            Host use = allocation.get(h);
            free.add(use);
        }

        return n;
        
    }

    
    protected boolean processConfig(JSONObject jsobj) throws JSONException {
        //JSONObject spaces = jsobj.getJSONObject("spaces");
        JSONObject spaces = jsobj;

        // get slice0
        JSONObject slice0Info = spaces.getJSONObject("slice0");

        // get slice0 hosts
        JSONArray slice0HArr = slice0Info.getJSONArray("hosts");

        // convert to a list of Host
        slice0 = new ArrayList<Host>();

        for (int i = 0; i < slice0HArr.length(); i += 1) {
            slice0.add(new Host(slice0HArr.optString(i)));
        }

        // get slice0 ports
        JSONArray slice0PArr = slice0Info.getJSONArray("ports");

        // convert to a list of ports (Integers)
        slice0Ports = new ArrayList<Integer>();

        for (int i = 0; i < slice0PArr.length(); i += 1) {
            slice0Ports.add(slice0PArr.optInt(i));
        }

        // get hosts for placing VMs onto
        JSONArray hostsArr = spaces.getJSONArray("hosts");

        // convert to a list of Host
        hosts = new ArrayList<Host>();

        for (int i = 0; i < hostsArr.length(); i += 1) {
            hosts.add(new Host(hostsArr.optString(i)));
        }

        return true;
    }

}
