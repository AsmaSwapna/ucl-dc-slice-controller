package slice.resource;

import java.util.List;

public interface ResourceManager {

    /**
     * Start the ResourceManager
     */
    public boolean start();
    
    /**
     * Stop the ResourceManager
     */
    public boolean stop();

    /**
     * How many hosts controlled by the ResourceManager
     */
    public int countHosts();

    /**
     * How many free hosts controlled by the ResourceManager
     */
    public int countFreeHosts();

    /**
     * How many used hosts controlled by the ResourceManager
     */
    public int countUsedHosts();

    /**
     * Get a host + port no to place a VIM
     * Pass in the no of hosts to be in the slice and 
     * the type of the VIM
     */
    public HostPort getVimHost(int sliceSize, String sliceType);
    
    /**
     * Get a list of N hosts for a slice
     */
    public List<Host> allocate(int n) throws AllocationException;

    /**
     * Confirm a list of N hosts pre allocated for a slice.
     * The hosts are marked as being used.
     * @param code the hashCode of the List returned by allocate()
     */
    public int confirm(int code);

    /**
     * Release a list of N hosts pre allocated for a slice.
     * The hosts are put back in the free pool.
     * @param code the hashCode of the List returned by allocate()
     */
    public int release(int code);

        /**
     * Free a list of N hosts which were allocated for a slice.
     * The hosts are put back in the free pool.
     */
    public int free(List<Host> allocation );
}
