package slice.resource;

/**
 * A manager for remote users
 */
public interface UserManager {

    /**
     * This finds the name of the remote user for a specific host
     */
    public String findRemoteUser();
}
