package slice.slice;

import java.util.List;
import java.util.Date;

import slice.resource.Host;
import slice.resource.HostPort;
import slice.vim.VimInfo;


public class BasicSlice implements Slice {
    // When the slcie was created
    Date creationTime;

    // The size of the slice
    int size;

    // The type of the slice
    String type;

    // The hosts for the vim   
    HostPort vimHost;

    // The hosts for the slice
    List<Host> allocation;

    // The Info for the VIM
    VimInfo vimInfo;
    

    

    public BasicSlice(int size, String type, HostPort vimHost,  List<Host> alloc, VimInfo vimInfo) {
        creationTime = new Date();
        this.size = size;
        this.type = type;
        this.vimHost = vimHost;
        this.allocation = alloc;
        this.vimInfo = vimInfo;
    }


        /**
     * Get the size of the slice.
     */
    public int getSize() {
        return size;
    }

    /**
     * Get the type of the slice.
     */
    public String getType() {
        return type;
    }

    /**
     * Get the Host and Port of the vim
     */
    public HostPort getVimHost() {
        return vimHost;
    }


    /**
     * Get the hosts allocated for the slice
     */
    public List<Host> getAllocation() {
        return allocation;
    }


    /**
     * Get the Info for the deployed VIM
     */
    public VimInfo getVimInfo() {
        return vimInfo;
    }

    /**
     * Get the slice ID
     */
    public int getID() {
        return hashCode();
    }


    public boolean equals(Object other) {
        if (other instanceof BasicSlice) {
            BasicSlice otherBS = (BasicSlice)other;
            
            return vimHost.equals(otherBS.vimHost);
        } else {
            return false;
        }
    }

    public int hashCode() {
        int val = (size + type.hashCode() + vimHost.hashCode() + creationTime.hashCode()) & 0x7fffffff;
        return val;
    }

    /**
     * toString
     */
    public String toString() {
        return "type: " + type + " size: " + size + " addr: " + vimHost + " allocation: " + allocation;
    }
}
