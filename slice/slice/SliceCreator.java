package slice.slice;

import java.util.List;

import us.monoid.json.JSONObject;

import slice.SliceController;
import slice.resource.ResourceManager;
import slice.resource.UserManager;
import slice.resource.Host;
import slice.resource.HostPort;
import slice.resource.AllocationException;
import slice.vim.VimFactory;
import slice.vim.VimInfo;

public class SliceCreator {

    // The SliceController
    SliceController controller;
    
    /**
     * Constructor.  Pass in the SliceController
     */
    public SliceCreator(SliceController controller) {
        this.controller = controller;
    }


    /**
     * Create a new slice given a JSONObject
     */
    public Slice create(JSONObject sliceRequest) throws IllegalArgumentException, AllocationException {

        // Get the ResourceManager
        ResourceManager resources = controller.getResourceManager();

        // Get the UserManager
        UserManager users = controller.getUserManager();

        // Process the request
        // it should look something liek this: '{"size":"2","type":"vlsp"}'

        int sliceSize = sliceRequest.optInt("size");
        String sliceType = sliceRequest.optString("type");

        // Find the username for the user who should start the remote processes
        String remoteUser = users.findRemoteUser();

        if (sliceSize <= 0) {
            throw new IllegalArgumentException("Slice size must be > 0");
        } else {

            // Check if we have the resources
            if (resources.countFreeHosts() >= sliceSize) {
                System.err.println("SliceCreator: Have enough resources");

                // Try and get some hosts for the slice 

                System.err.println("SliceCreator: allocate " + sliceSize);

                List<Host> allocation = resources.allocate(sliceSize);


                // Find a place for the VIM whcih will manage the allocated hosts of the slice
                HostPort vimHostPort = resources.getVimHost(sliceSize, sliceType);

                System.err.println("Slice vim host " + vimHostPort + " allocation code = " + allocation.hashCode() + " hosts = " + allocation);

                // Now try to Instantiation the slice
                // using the relevant context info
                VimInfo instantiated = VimFactory.instantiate(sliceType, vimHostPort, allocation, remoteUser, controller.getRemoteConfig());

                // If the instantiation works, carry on
                // otherwise release the resources
                if (instantiated == null) {
                    // could not instantiate the  VIM
                    // release the hosts
                    System.err.println("SliceCreator: release " + allocation);

                    resources.release(allocation.hashCode());

                    
                    return null;
                
                } else {
                    System.err.println("SliceCreator: confirm " + allocation + " instantiated = " + instantiated);

                    resources.confirm(allocation.hashCode());


                    // Now create a Slice object with all the relevant details
                    // and a handle on the process
                    Slice slice = new BasicSlice(sliceSize, sliceType, vimHostPort, allocation, instantiated);

                    return slice;
                }

                
            } else {
                System.err.println("NOT enough resources");
                // Not enough resource
                throw new AllocationException("Not enough hosts");
            }
        }
    }

}
