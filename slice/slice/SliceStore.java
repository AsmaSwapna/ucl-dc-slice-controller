package slice.slice;

import java.util.Set;

public interface SliceStore {
    /**
     * Add a slice
     * Returns a unique code
     */
    public int add(Slice s);

    /**
     * Get a slice by ID
     * Returns the slice
     */
    public Slice get(int sliceID);

    /**
     * Remove a slice
     * Returns the slice
     */
    public Slice remove(Slice s);

    /**
     * Remove a slice by ID
     * Returns the slice
     */
    public Slice remove(int sliceID);

    /**
     * Count the number of slices
     */
    public int count();

    
    /** 
     * Get a set of all the slice IDs
     */
    public Set<Integer>	getSliceIDs();

}
