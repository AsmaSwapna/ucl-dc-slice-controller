package slice.vim;

import com.github.mustachejava.DefaultMustacheFactory; 
import com.github.mustachejava.Mustache; 
import com.github.mustachejava.MustacheFactory;

import java.util.Map;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;



/**
 * A template engine based on mustache
 */
public class MustacheTemplateEngine implements TemplateEngine {
    MustacheFactory mf;

    public MustacheTemplateEngine() {
        mf = new DefaultMustacheFactory();
    }

    /**
     * Generate a file froma template and some info.
     * Returns the Generated filename.
     */
    public String generate(String templatePath, Map<String, Object> info, String dstPath, String dstFileName) throws IOException {
        Mustache template = mf.compile(templatePath);

        File outputFile;

        if (dstPath == null) {
            // user current dir
            outputFile = new File(dstFileName);
        } else {
            outputFile = new File(dstPath, dstFileName);
        }
 
        FileWriter writer = new FileWriter(outputFile);
        template.execute(writer, info);

        writer.close();

        return outputFile.getPath();
            
    }
}
