package slice.vim;

import java.util.List;
import slice.resource.Host;
import slice.resource.HostPort;

import us.monoid.json.JSONObject;

/** 
 * An interface for classes that can start an instance of a VIM
 */
public interface VimInitiator {
    /**
     * Instantiate a new Vim.
     * @param type  the type of the Vim
     * @param vimHostPort the port the new Vim should listen on
     * @param allocation the list of hosts the new Vim will have control of
     * @param remoteUser the username of a user who can connect using ssh to the host running the Vim
     * @param remoteConfig a JSONObject of other config args ised during setup
     */
    public VimInfo instantiate(String type, HostPort vimHostPort, List<Host> allocation, String remoteUser, JSONObject remoteConfig);

}
