package slice.vim;

import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.net.SocketException;
import java.net.InetAddress;
import java.io.IOException;
import java.util.Properties;
import java.util.List;
import java.util.Arrays;
import java.util.HashMap;

import cc.clayman.logging.Logger;
import cc.clayman.logging.MASK;

import slice.resource.Host;
import slice.resource.HostPort;

import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;
import us.monoid.web.Resty;

/** 
 * This will start a VLSP instance 
 */
class VlspInitiator implements VimInitiator {
    // Create a set of config files for this instantiation
    String vlspTemplateFilePath = "templates/vlsp/control-template.moustache";
    String routerOptionsTemplateFilePath = "templates/vlsp/routeroptions.moustache";

    

    public VlspInitiator() {
    }

    public VimInfo instantiate(String type, HostPort vimHostPort, List<Host> allocation, String remoteUser, JSONObject remoteConfig) {

        Logger.getLogger("log").logln(MASK.STDOUT, leadin() + "Starting VIM on host " + vimHostPort +" with allocation "+ allocation);

        Process child = null;
        ProcessWrapper pw = null;
        String[] cmd = {};


        
        try {
            
            String hostname = vimHostPort.getName();
            int port = vimHostPort.getPort();
            int monitoringPort = port + 20000;

            String path = null;

            if (remoteConfig != null) {
                path = remoteConfig.optString("path");
            }


            String vlspConfigFileName = "control-" + vimHostPort.getName() + "-" + vimHostPort.getPort() + ".xml";
            String routerOptionsConfigFileName = "routeroptions-" + vimHostPort.getName() + "-" + vimHostPort.getPort() + ".xml";



            // now get the template engine to create new files
            // based on the current context info
            TemplateEngine templateEngine = new MustacheTemplateEngine();

            HashMap<String, Object> context = new HashMap<>(); 
            context.put("port", Integer.toString(port)); 
            context.put("host", hostname);
            context.put("vimHosts", allocation);
            context.put("monitoringPort", Integer.toString(monitoringPort));
            context.put("routerOptionsFile", routerOptionsConfigFileName);

            Logger.getLogger("log").logln(MASK.STDOUT, leadin() + "Generate template from: " + vlspTemplateFilePath + " info: " + context + " to: " + path + " / " + vlspConfigFileName);


             String vlspConfigFile = templateEngine.generate(vlspTemplateFilePath, context, path, vlspConfigFileName);

             String routerOptionsConfigFile = templateEngine.generate(routerOptionsTemplateFilePath, null, path, routerOptionsConfigFileName);

            Logger.getLogger("log").logln(MASK.STDOUT, leadin() + "Generate template into " + vlspConfigFileName);
            

            // now construct Map of values
            JSONObject options = new JSONObject();
            options.put("remote-host", hostname);
            options.put("remote-user", remoteUser);
            options.put("config-path", vlspConfigFile);

            cmd = startCommand(options);

            Logger.getLogger("log").logln(MASK.ERROR, leadin() + "cmd = "+ Arrays.asList(cmd));
            
            child = new ProcessBuilder(cmd).start();
            pw = new ProcessWrapper(child, vimHostPort.getName()+"-"+vimHostPort.getPort());



            // Create the VimInfo
            VimInfo vimInfo = new VimInfo();
            vimInfo.processWrapper = pw;
            vimInfo.hostname = hostname;
            vimInfo.port = Integer.toString(port);
            vimInfo.username = remoteUser;
            vimInfo.vimConfigFile = vlspConfigFile;




            // Now we sit in a loop
            // and wait to see if it ready
            Resty rest = new Resty();
            String vimURI = "http://" + hostname + ":" + Integer.toString(port);
            String uri = vimURI + "/command/STATUS";


            Logger.getLogger("log").logln(MASK.ERROR, leadin() + "vimURI = " + vimURI);

            // keep a track of IO errors
            int ioErrors = 0;
            
            while (true) {
                if (ioErrors == 40) {
                    Logger.getLogger("log").logln(MASK.ERROR, leadin() + "too many IO errors");;
                    throw new Exception("too many IO errors");
                }

                // sleep 500ms
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ie) {
                }

                
                try {
                    // Get the status
                    JSONObject jsobj = rest.json(uri).toObject();

                    // check it
                    String status = jsobj.optString("status");

                    Logger.getLogger("log").logln(MASK.ERROR, leadin() + "Current status: " + status);

                    if (status.equals("RUNNING")) {
                        
                        return vimInfo;
                    }
                } catch (IOException ioe) {
                    ioErrors++;
                }
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
            Logger.getLogger("log").logln(MASK.ERROR, ioe.getMessage());


            if (child != null) {
                child = null;
            }

            if (pw != null) {
                pw.stop();
            }

            return null;

        } catch (Exception e) {
            //e.printStackTrace();
            Logger.getLogger("log").logln(MASK.ERROR, "Exception : " + e.getMessage());


            if (child != null) {
                child = null;
            }

            if (pw != null) {
                pw.stop();
            }

            return null;
        } catch (Error ee) {
            ee.printStackTrace();
            Logger.getLogger("log").logln(MASK.ERROR, ee.getMessage());


            if (child != null) {
                child = null;
            }

            if (pw != null) {
                pw.stop();
            }

            return null;
        }



    }
    

    // Create a String[] to pass into a ProcessBuilder
    // this works out of the command is local or not
    private String [] startCommand(JSONObject options) throws Exception {
        boolean isLocal = false;
        InetAddress addr = null;

        String host = options.optString("remote-host");

        if (host == null) {
            throw new Exception("No 'remote-host' option specified for VlspInitiator");
        } else {
            try {
                addr = InetAddress.getByName(host);
            } catch (UnknownHostException uhe) {
                throw new Exception(uhe.getMessage());
            }
        
            // Check if the address is a valid special local or loop back
            if (addr.isAnyLocalAddress() || addr.isLoopbackAddress()) {
                isLocal = true;
        
            } else {
                // Check if the address is defined on any interface
                try {
                    isLocal = NetworkInterface.getByInetAddress(addr) != null;
                } catch (SocketException e) {
                    isLocal = false;
                }
            }

            if (isLocal) {

                // no need to do remote command
                String classpath = System.getProperty("java.class.path");

                String [] cmd = new String[5];
                cmd[0] = "java";
                cmd[1] = "-classpath";
                cmd[2] = classpath;
                cmd[3] = "usr.vim.Vim";
                cmd[4] = options.optString("config-path");


                return cmd;

            } else {
                // its a remote command

                String remoteLoginCommand = "ssh";     // Command used  to login to start local controller
                String remoteLoginFlags = "-f";        //  Flags used for ssh to login to remote machine
                Properties prop = System.getProperties();
                String remoteStartController = "java -cp " + prop.getProperty("java.class.path", null) + " usr.vim.Vim";
        
            
                String [] cmd = new String[5];
                cmd[0] = remoteLoginCommand;
                cmd[1] = remoteLoginFlags;

                // For user name in turn try info from remote, or info
                // from global or fall back to no username
                String user = options.optString("remote-user");

                if (user == null) {
                    cmd[2] = host;
                } else {
                    cmd[2] = user + "@" + host;
                }

                String remote = remoteStartController;

                cmd[3] = remote;
                cmd[4] = options.optString("config-path");

                return cmd;
            }
        }
    }


    public String leadin() {
        return "VlspInitiator: ";
    }
}
